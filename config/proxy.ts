export default {
  dev: {
    '/api': {
      // 要代理的地址
      target: 'https://creationapi.shbwyz.com',
      changeOrigin: true,
      pathRewrite: { '^/api': '' },
    },
  },
};
