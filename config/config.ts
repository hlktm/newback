// config/config.ts

import { defineConfig } from 'umi';
import routes from './routes';
import proxy from './proxy';
export default defineConfig({
  routes: routes,
  layout: {
    name: '后台管理',
    logo: 'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png',
  },
  scripts: [
    'https://unpkg.com/react@17/umd/react.production.min.js',
    'https://unpkg.com/react-dom@17/umd/react-dom.production.min.js',
    'https://unpkg.com/@ant-design/charts@1.0.5/dist/charts.min.js',
    //使用 组织架构图、流程图、资金流向图、缩进树图 才需要使用
    'https://unpkg.com/@ant-design/charts@1.0.5/dist/charts_g6.min.js',
  ],
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    '@ant-design/charts': 'charts',
  },
  dva: {
    immer: true,
    hmr: false,
  },
  proxy: proxy['dev'],
});
