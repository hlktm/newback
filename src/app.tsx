import React from 'react';
import { RequestConfig, history } from 'umi';
import { message } from 'antd';
import { Menu, Dropdown, Space, Button, Popover } from 'antd';
import { DownOutlined, SmileOutlined } from '@ant-design/icons';
import styles from './app.less';
import {
  BasicLayoutProps,
  Settings as LayoutSettings,
} from '@ant-design/pro-layout';
import Footer from './components/footer';
import Right from './components/right';
import MenuHeader from './components/menuHeader';
import { createLogger } from 'redux-logger';
export const layout = () => {
  const content = (
    <div>
      <p>
        <a href="/personal">新建文章-协同编辑器</a>
      </p>
      <p>
        <a href="/article/editor"> 新建文章</a>
      </p>
      <p>
        <a href="/article/editor"> 新建页面</a>
      </p>
    </div>
  );
  return {
    rightContentRender: () => <Right />,
    footerRender: () => <Footer />,
    menuHeaderRender: () => <MenuHeader />,

    onMenuHeaderClick: (e: any) => {
      console.log(e);
      e //阻止默认事件
        //preventDefault()[dom标准写法(ie678不兼容)]
        //ie678用returnValue
        //或者利用return false也能阻止默认行为,没有兼容问题(只限传统注册方式)
        .preventDefault();
      e.stopPropagation();
    },
    onPageChange: () => {
      history.listen((item) => {
        // console.log(item);
      });
      // console.log(history.location.pathname);
    },
  };
};
export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    // 请求前拦截器  检验身份
    (url: any, options: any) => {
      // console.log("请求接口前执行，处理请求接口是携带的公共参数");
      const headers = {
        ...options.headers,
      };

      const user: any =
        localStorage.getItem('user') &&
        JSON.parse(localStorage.getItem('user')!);
      if (!options.isAuthorization && user) {
        // 获取本地存储
        headers.authorization = `Bearer  ${user && user.token}`;
      }
      // console.log(options, "header");
      // console.log(headers, "header");
      return {
        url: url,
        options: {
          //请求头  请求参数  请求方式
          ...options,
          interceptors: true,
          headers,
        },
      };
    },
  ],
  responseInterceptors: [
    (response: any) => {
      const codeMaps = {
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
        401: '请先登录。',
        403: '没有访问权限。',
      };
      if (response.status == 200 || response.status == 304) {
        return response;
      }

      message.error(codeMaps[response.status]);
      return;
    },
  ],
};

export const getInitialState = () => {
  // console.log('getInitialState');
  let userInfo = localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user')!)
    : { role: 'admin' };
  // console.log(userInfo, 'userInfo');
  return userInfo;
};
export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};
