import { Redirect } from 'umi';

export default (props: any) => {
  // console.log(props, 'pros');

  const isLogin = localStorage.getItem('user');
  if (isLogin) {
    return <div>{props.children}</div>;
  } else {
    return <Redirect to="/login" />;
  }
};
