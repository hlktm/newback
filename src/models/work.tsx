import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { commentList, commentChange } from '../services/comments';
export interface IndexModelState {
  [name: string]: any;
}

export interface IndexModelType {
  namespace: 'work';
  state: IndexModelState;
  effects: {
    workGetList: Effect;
  };
  reducers: {
    save: Reducer<IndexModelState>;
    workList: Reducer<IndexModelState>;
    changeTrue: Reducer<IndexModelState>;

    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
  subscriptions: { setup: Subscription };
}

const IndexModel: IndexModelType = {
  namespace: 'work',

  state: {
    name: '555',
    workComment: '',
    workCommentLength: '',
  },

  effects: {
    *workGetList({ payload }, { call, put }) {
      let res = yield call(commentList);
      // console.log(res,"res");
      yield put({
        type: 'workList',
        payload: res.data,
      });
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    workList(state, action) {
      return {
        ...state,
        workComment: action.payload[0],
        workCommentLength: action.payload[1],
      };
    },
    changeTrue(state, action) {
      console.log(action, 'action');
      console.log(state, 'state');
      console.log(this, 'this');

      return {
        ...state,
        // workComment: action.payload[0],
        // workCommentLength: action.payload[1],
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/') {
          dispatch({
            type: 'query',
          });
        }
      });
    },
  },
};

export default IndexModel;
