// import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
// import { ArticleList } from '@/services/article';
// export interface ArticleModelState {
//   [title: string]: any;
// }
// export interface ArticleModelType {
//   namespace: 'article';
//   state: ArticleModelState;
//   effects: {
//     ArticleList: Effect;
//   };
//   reducers: {
//     changeTitle: Reducer<ArticleModelState>;
//     // 启用 immer 之后
//     // save: ImmerReducer<IndexModelState>;
//   };
// }
// const article: ArticleModelType = {
//   namespace: 'article',
//   state: {
//     title: '123',
//   },
//   effects: {
//     *ArticleList({ payload }, { call, put }) {
//       let res = yield call(ArticleList);
//       yield put({
//         type: 'ArticleList',
//         payload: res.data,
//       });
//     },
//   },
//   reducers: {
//     changeTitle(state, action) {
//       console.log(state, action, 'state,action');

//       return {
//         ...state,
//         ...action.payload,
//       };
//     },
//     // 启用 immer 之后
//     // save(state, action) {
//     //   state.name = action.payload;
//     // },
//   },
// };
// export default article;
