// src/access.ts
export default function (initialState: any) {
  const { role } = initialState;
  // console.log(initialState);

  // console.log(role, 'role');

  return {
    canLogin: role === 'admin',
  };
}
