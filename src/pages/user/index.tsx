import styles from './index.less';
import Breadcrumb from '../../components/breadcrumb';
import React, { useState } from 'react';
import { Table, Button } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import RedoOutlined from '@ant-design/icons/lib/icons/RedoOutlined';
import { Form, Input, Select } from 'antd';
const { Option } = Select;
type Currency = 'rmb' | 'dollar';
interface PriceValue {
  number?: number;
  currency?: Currency;
}
interface PriceInputProps {
  value?: PriceValue;
  onChange?: (value: PriceValue) => void;
}
interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}
const columns: ColumnsType<DataType> = [
  {
    title: '账户',
    dataIndex: 'name',
  },
  {
    title: '邮箱',
    dataIndex: 'age',
  },
  {
    title: '角色',
    dataIndex: 'address',
  },
  {
    title: '类型',
    dataIndex: 'address',
  },
  {
    title: '状态',
    dataIndex: 'address',
  },
  {
    title: '注册日期',
    dataIndex: 'address',
  },
  {
    title: '操作',
    dataIndex: 'address',
  },
];
const data: DataType[] = [];
export default function IndexPage() {
  const onFinish = (values: any) => {
    console.log('Received values from form: ', values);
  };
  const checkPrice = (_: any, value: { number: number }) => {
    if (value.number > 0) {
      return Promise.resolve();
    }
    return Promise.reject(new Error('Price must be greater than zero!'));
  };
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false);
  const start = () => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
  };
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    // console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
  return (
    <div>
      <Breadcrumb children={{ title: '用户管理', path: '/user' }} />
      <Form
        className={styles.from}
        name="customized_form_controls"
        layout="inline"
        onFinish={onFinish}
        initialValues={{
          price: {
            number: 0,
            currency: 'rmb',
          },
        }}
      >
        <Form.Item name="price" rules={[{ validator: checkPrice }]}>
          <ul className={styles.soulis}>
            <li className={styles.sou}>
              <span>账户：</span>
              <Input
                className={styles.souin}
                type="text"
                placeholder="请输入用户账户"
              />
            </li>
            <li className={styles.sou}>
              <span>邮箱：</span>
              <Input
                className={styles.souin}
                type="text"
                placeholder="请输入账户邮箱"
              />
            </li>
            <li className={styles.sou}>
              <span>角色:</span>
              <p>
                <Select style={{ width: 250, margin: '0 8px' }}>
                  <Option value="rmb">管理员</Option>
                  <Option value="dollar">访客</Option>
                </Select>
              </p>
            </li>
            <li className={styles.sou}>
              <span>状态:</span>
              <p>
                <Select style={{ width: 250, margin: '0 8px' }}>
                  <Option value="rmb">锁定</Option>
                  <Option value="dollar">可用</Option>
                </Select>
              </p>
            </li>
            <li className={styles.sou}>
              <span>账户：</span>
              <Input
                className={styles.souin}
                type="text"
                placeholder="请输入用户类型"
              />
            </li>
          </ul>
        </Form.Item>
        <Form.Item className={styles.buttons}>
          <div></div>
          <div>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
            &nbsp;&nbsp;
            <Button>重置</Button>
          </div>
        </Form.Item>
      </Form>
      <div className={styles.mainlist}>
        <div style={{ marginBottom: 16 }} className={styles.refreshdiv}>
          <RedoOutlined className={styles.refresh} />
        </div>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
        />
      </div>
    </div>
  );
}
