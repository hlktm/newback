import styles from './index.less';
import React, { useEffect, useState } from 'react';
import { Drawer, Col, Row, DatePicker } from 'antd';
import Card from '../../components/fcard/index';
import Draw from '../../components/draw/index';
import { PlusOutlined } from '@ant-design/icons';
import { useRequest } from 'umi';
import { fileList } from '../../services/file';
import UpLoad from '../../components/upload';
import Breadcrumb from '../../components/breadcrumb';
import { UploadOutlined, InboxOutlined } from '@ant-design/icons';
import {
  Table,
  Tag,
  Space,
  Button,
  Form,
  Input,
  Checkbox,
  Select,
  Pagination,
  Switch,
} from 'antd';

export default function IndexPage() {
  const [visible, setVisible] = useState(false);
  // const [Data, setData] = useState([]);
  const { data, error, loading } = useRequest(fileList);

  // console.log(data && data[0], 'daa');
  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 22 },
  };
  //打开抽屉
  const showDrawer = () => {
    setVisible(true);
  };
  //关闭抽屉
  const onClose = () => {
    setVisible(false);
  };
  const onFinish = (values: any) => {
    console.log(values);
  };

  useEffect(() => {
    // console.log(data,'data');
    // if (data && data) {
    //   setData(aaa);
    //   console.log(Data);
    // }
  }, []);
  return (
    <div className={styles.knowledge_box}>
      <Breadcrumb children={{ title: '文件管理', path: '/files' }} />
      <div className={styles.head}>
        <UpLoad />
      </div>
      <div className={styles.header_box}>
        <div className={styles.input_box}>
          发件人:
          <Input placeholder="请输入发件人" style={{ width: '230px' }} />
          收件人:
          <Input placeholder="请输入收件人" style={{ width: '230px' }} />
        </div>
        <div className={styles.button_box}>
          <Button type="primary" style={{ marginRight: '30px' }}>
            搜索
          </Button>
          <Button>重置</Button>
        </div>
      </div>

      <div className={styles.content_box}>
        {/* 新建 */}
        <div className={styles.content_head}>
          <div></div>
        </div>
        <div className={styles.content}>
          {/* {data &&
            data[0].map((item: any, index: number) => {
              return (
                <div key={index} className={styles.dl}>
                  <dl onClick={showDrawer}>
                    <dt>
                      <img
                        src={item.url}
                        alt=""
                        style={{ width: 250, height: 180 }}
                      />
                    </dt>
                    <dd>
                      <p>{item.originalname}</p>
                      <p>上传于{item.createAt}</p>
                    </dd>
                  </dl>
                </div>
              );
            })} */}
          <Card
            list={data && data[0]}
            isAction={true}
            onShow={showDrawer}
          ></Card>

          <div className={styles.footer_box}>
            <div></div>
            <Pagination
              total={100}
              current={1}
              pageSize={12}
              showSizeChanger={true}
            />
          </div>
        </div>
      </div>

      {/* 新建抽屉 */}
      <Draw visible={visible} changeShowDrawer={onClose} />
    </div>
  );
}
