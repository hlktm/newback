import React, { useEffect, useState } from 'react';
import { useRequest, Link } from 'umi';
import { Input, Form, Popover, message, Popconfirm } from 'antd';
import { list } from '../../services/list';
import {
  commentList,
  commentChange,
  commentDel,
} from '../../services/comments';
import Login from '../login';
import Breadcrumb from '../../components/breadcrumb';
import styles from './index.less';
import Charts from '../../components/charts';
import { Space, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/lib/table';

import {
  IndexModelState,
  ConnectRC,
  Loading,
  connect,
  useDispatch,
  useSelector,
} from 'umi';

import {
  PageProps,
  ListType,
  DataType,
  ItemType,
  dvaStateType,
  dvaNameType,
  dvaItemType,
} from '../../types/work';
import TableTwo from '../../components/table/tableTow';
const celerityData = [
  {
    title: '文章管理',
    path: '/article/allarticles',
  },
  {
    title: '评论管理',
    path: '/comments',
  },
  {
    title: '文件管理',
    path: '/files',
  },
  {
    title: '用户管理',
    path: '/user',
  },
  {
    title: '访问管理',
    path: '/access',
  },
  {
    title: '系统设置',
    path: '/system',
  },
];
const Page: React.FC<ListType> = () => {
  let dispatch = useDispatch();
  // 进入页面异步请求数据
  useEffect(() => {
    dispatch({
      type: 'work/workGetList',
    });
  }, []);
  let state: dvaStateType = useSelector((state: any) => state.work);
  // 通过
  const refusePass = (text: dvaItemType) => {
    text.pass = true;
    console.log(text, 'txet');
    commentChange(text)
      .then((res) => {
        dispatch({
          type: 'work/workGetList',
        });
        message.success('评论已通过');
      })
      .catch((res) => {
        console.log(res);

        message.error(res.msg);
      });
  };
  // 拒绝
  const refuseNoPass = (text: dvaItemType) => {
    text.pass = false;
    console.log(text, 'txet');
    commentChange(text)
      .then((res) => {
        dispatch({
          type: 'work/workGetList',
        });
        message.success('评论已拒绝');
      })
      .catch((res) => {
        console.log(res);

        message.error(res.msg);
      });
  };

  // 确认删除
  function sureDel() {}
  const refuseDel = (text: dvaItemType) => {
    console.log(text, 'txet');
  };
  // 取消删除
  const cancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e, 'cancel');
    message.error('取消删除');
  };
  // 删除
  const confirm = (text: any) => {
    // refuseDel
    commentDel(text)
      .then((res) => {
        dispatch({
          type: 'work/workGetList',
        });
        message.success('已删除');
      })
      .catch((res) => {
        console.log(res);

        message.error(res.msg);
      });
  };

  // commentChange
  const { data } = useRequest(list);
  // const commentData = useRequest(commentList).data;
  const commentData = state.workComment;
  // console.log(useRequest(list));
  // console.log(data, "DATA");
  // console.log(commentData&&commentData[0], "commentData");
  let datalist =
    commentData &&
    commentData
      .map((element: any, index: number) => {
        // console.log(element,"element");
        return { ...element, ...{ key: element.id } };
      })
      .slice(0, 6);

  const columns: ColumnsType<DataType> = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '70%',
      render: (_, record) => (
        <div>
          {record.name}在
          <Popover
            placement="right"
            content={
              <div className={styles.error_style}>
                <h1>404</h1>{' '}
                <div style={{ fontSize: '30px', paddingLeft: '15px' }}>|</div>
                <h2>This page could not be found</h2>
              </div>
            }
            title="页面预览"
          >
            <a>文章</a>
          </Popover>
          评论
          <Popover
            content={
              <div>
                <p>{record.content}</p>
              </div>
            }
            title="评论详情-原始内容"
          >
            <a>查看内容</a>
          </Popover>{' '}
          {record.pass ? (
            <b>
              <span className={styles.span_style}></span>
              <span>通过</span>
            </b>
          ) : (
            <b>
              <span className={styles.spans_style}></span>
              <span>未通过</span>
            </b>
          )}{' '}
        </div>
      ),
    },

    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => refusePass(_)}>通过</a>
          <a onClick={() => refuseNoPass(_)}>拒绝</a>
          <a>回复</a>
          <Popconfirm
            title="确定删除这个评论"
            onConfirm={() => confirm(_)}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a>删除</a>
            {/* <a >删除</a> */}
          </Popconfirm>
        </Space>
      ),
    },
  ];
  return (
    <div className={styles.work}>
      <Breadcrumb />
      <div className={styles.charts}>
        <div className={styles.head}>
          <span>面板导航</span>
        </div>
        <Charts />
      </div>
      <div className={styles.celerity}>
        <div className={styles.head}>
          <span>快速导航</span>
        </div>
        <div className={styles.tap}>
          {celerityData.map((item, index) => {
            return (
              <span key={index}>
                <Link to={item.path}>{item.title}</Link>
              </span>
            );
          })}
        </div>
      </div>
      <div className={styles.article}>
        <div className={styles.head}>
          <span>最新文章</span>
          <span>
            <Link to="/article/allarticles">全部文章</Link>
          </span>
        </div>
        <div className={styles.tap}>
          {data &&
            data[0].slice(0, 6).map((item: ItemType, index: number) => {
              // console.log(item);
              return (
                <dl key={index}>
                  <dd>
                    <img src={item.cover} alt="文章封面" />
                  </dd>
                  <dt>{item.title}</dt>
                </dl>
              );
            })}
        </div>
      </div>
      <div className={styles.article}>
        <div className={styles.head}>
          <span>最新评论</span>
          <span>
            <Link to="/comments">全部评论</Link>
          </span>
        </div>
        <div className={styles.tap}>
          <TableTwo
            columns={columns}
            balbe={{
              rowKey: 'id',
            }}
            dataSource={datalist}
            showHeader={false}
            pagination={false}
          />
          {/* <Table 
          columns={columns} 
         
          dataSource={datalist} 
          showHeader={false} 
          pagination={false} 
          /> */}
        </div>
      </div>
    </div>
  );
};
export default Page;
