import React, { useEffect } from 'react';
import { useRequest, useHistory, useModel } from 'umi';
import { Form, Input, Button, Image, message } from 'antd';
import { login } from '../../services/login';
import LoginPng from '../../imgs/login.png';
import styles from './index.less';
const App: React.FC = () => {
  useEffect(() => {
    localStorage.getItem('user') && localStorage.removeItem('user');
  }, []);

  const history = useHistory();
  const { run, loading } = useRequest(login, { manual: true });
  const { setInitialState } = useModel('@@initialState');
  const onFinish = async (values: any) => {
    try {
      let res = await run(values);
      console.log(values, 'values');
      console.log(res, 'res');

      setInitialState(res);
      // 存储用户名
      localStorage.setItem('user', JSON.stringify(res));

      history.push('/');
    } catch (error) {
      message.error('账号或密码错误');
    }
  };
  const onFinishFailed = (errorInfo: any) => {
    // console.log('Failed:', errorInfo);
  };
  return (
    <div className={styles.loginAll}>
      <div className={styles.loginLeft}>
        <img src={LoginPng} alt="" />
      </div>
      <div className={styles.from}>
        <div className={styles.loginCon}>
          <h1>系统登录</h1>
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            labelAlign="left"
          >
            <Form.Item
              label="账户"
              name="name"
              labelCol={{ span: 3, offset: 0 }}
              wrapperCol={{ span: 20, offset: 0 }}
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="密码"
              name="password"
              labelCol={{ span: 3, offset: 0 }}
              wrapperCol={{ span: 20, offset: 0 }}
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0, span: 23 }}>
              <Button
                type="primary"
                htmlType="submit"
                style={{ width: '100%' }}
              >
                登录
              </Button>
            </Form.Item>
          </Form>
          <p>
            Or <a href="/registry">注册用户</a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default App;
