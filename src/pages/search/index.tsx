import styles from './index.less';
import { useRequest } from 'umi';
import React, { useState } from 'react';
import Breadcrumb from '../../components/breadcrumb';
import { searchList } from '../../services/search';
import { Table, Button, Input, Badge } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import type { ColumnsType } from 'antd/lib/table';

interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
  views: number;
  title: string;
  createAt: string;
}

const columns: ColumnsType<DataType> = [
  {
    title: '搜索词',
    width: 100,
    dataIndex: 'title',
    key: 'title',
    fixed: 'left',
  },

  {
    title: '搜索量',
    dataIndex: 'views',
    key: 'views',
    width: 150,
    render: (text, record) => <span className={styles.numbers}>{text}</span>,
  },
  {
    title: '搜索时间',
    dataIndex: 'createAt',
    key: 'createAt',
    width: 150,
  },

  {
    title: '操作',
    key: 'operation',
    fixed: 'right',
    width: 100,
    align: 'center',
    render: () => <a>删除</a>,
  },
];
export default function IndexPage() {
  const { data, error, loading } = useRequest(searchList);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  // const hasSelected = selectedRowKeys.length > 0;
  function onSelectChange(newSelectedRowKeys: React.Key[]) {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  return (
    <div>
      <Breadcrumb children={{ title: '搜索记录', path: '/search' }} />
      <div className="head">
        <div className={styles.header_box}>
          <div className={styles.input_box}>
            类型:
            <Input placeholder="请输入搜索类型" style={{ width: '230px' }} />
            搜索词:
            <Input placeholder="请输入搜索词" style={{ width: '230px' }} />
            搜索量:{' '}
            <Input placeholder="请输入搜索量" style={{ width: '230px' }} />
          </div>
          <div className={styles.button_box}>
            <Button type="primary" style={{ marginRight: '30px' }}>
              搜索
            </Button>
            <Button>重置</Button>
          </div>
          <div className={styles.ioo}>
            <ReloadOutlined />
          </div>
        </div>
      </div>
      <Table
        rowSelection={rowSelection}
        columns={columns}
        pagination={{ pageSize: 2, showSizeChanger: true }}
        dataSource={data && data[0]}
        scroll={{ x: 1500, y: 300 }}
      />
    </div>
  );
}
