import styles from './index.less';
import { Form, Input, Row, Col, Button, Empty, Pagination } from 'antd';
import UpLoad from '../../components/upload';
import Breadcrumb from '../../components/breadcrumb';
import { postersList } from '../../services/posters';
import { fileList } from '@/services/file';
import { useState, useEffect } from 'react';
import Card from '../../components/posterCard';
import Pages from '../../components/pagination';
export default function IndexPage() {
  // const { run, loading } = useRequest(postersList, { manual: true });
  // const cliinn = async () => {
  //   let res = await run();
  //   console.log(res);
  // };
  let [page, setpage] = useState(1);
  let [pageSize, setPageSize] = useState(12);
  let [total, setTotal] = useState(50);
  let [isLook, setIsLook] = useState(true);
  let [fileData, setFileData] = useState<any>([[], 0]);

  useEffect(() => {
    fileList({ page, pageSize }).then((res: any) => {
      console.log(res, 'res');
      setFileData(res.data);
      setTotal(res.data[1]);
    });
  }, [page, pageSize]);
  const [form] = Form.useForm();

  console.log(fileData, 'fileData');

  const onFinish = ({ filename }: any) => {
    console.log(filename);
  };
  const changePage = (page: number, pageSize: any) => {
    console.log(page, pageSize, 'pageSize');
    setpage(page);
    setPageSize(pageSize);
    // getFile({ page, pageSize })
  };

  return (
    <div>
      <Breadcrumb children={{ title: '海报管理', path: '/breadcrumb' }} />
      <div className={styles.content}>
        <div className={styles.upload}>
          <UpLoad />
        </div>
        <div className={styles.from}>
          <Form onFinish={onFinish} form={form}>
            <Form.Item
              name="filename"
              label="文件名称"
              wrapperCol={{ span: '5' }}
            >
              <Input placeholder="请输入文件名称" />
            </Form.Item>
            {/* <Row gutter={24}>{getFields()}</Row> */}
            <Row>
              <Col span={24} style={{ textAlign: 'right' }}>
                <Button type="primary" htmlType="submit">
                  搜索
                </Button>
                <Button
                  style={{ margin: '0 8px' }}
                  onClick={() => {
                    form.resetFields();
                  }}
                >
                  重置
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
        <div className={styles.confoot}>
          {fileData ? (
            <Card list={fileData} />
          ) : (
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
          )}
        </div>
        <div className={styles.page}>
          <Pages
            current={page}
            total={total}
            pageSize={pageSize}
            isLook={true}
            looktotal={true}
            changePage={changePage}
          />
        </div>
      </div>
    </div>
  );
}
