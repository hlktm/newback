import styles from './index.less';
import React, { useEffect, useState, useRef } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import { useRequest } from 'umi';
import { knowledgeList } from '../../services/knowledge';
import Breadcrumb from '@/components/breadcrumb/index';
import { Button, Input, Select, Pagination } from 'antd';
import Drawers from '@/components/drawer/index';
import MyCard from '@/components/card/index';
const { Option } = Select;
export default function IndexPage() {
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState([]);
  const [selValue, setSelValue] = useState('');
  const MyForm = useRef(null) as any;
  const { run } = useRequest(knowledgeList, { manual: true });
  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 22 },
  };
  //打开抽屉
  const showDrawer = () => {
    setVisible(true);
  };
  //关闭抽屉
  const onClose = () => {
    setVisible(false);
  };
  //渲染数据
  const renderList = () => {
    run().then((res) => {
      setData(res[0]);
    });
  };
  useEffect(() => {
    renderList();
  }, []);
  return (
    <div>
      <Breadcrumb children={{ title: '知识小册', path: '/knowledge' }} />
      <div className={styles.knowledge_box}>
        <div className={styles.header_box}>
          {/* 头部搜索 */}
          <div className={styles.input_box}>
            名称:
            <Input placeholder="请输入知识库名称" style={{ width: '230px' }} />
            状态:
            <Select
              defaultValue=""
              style={{ width: 150 }}
              // onChange={handleChange}
            >
              <Option value="已发布">已发布</Option>
              <Option value="草稿">草稿</Option>
            </Select>
          </div>
          <div className={styles.button_box}>
            <Button type="primary" style={{ marginRight: '30px' }}>
              搜索
            </Button>
            <Button>重置</Button>
          </div>
        </div>
        <div className={styles.content_box}>
          {/* 新建 */}
          <div className={styles.content_head}>
            <div></div>
            <div>
              <Button
                type="primary"
                style={{ marginRight: '30px' }}
                onClick={showDrawer}
                icon={<PlusOutlined />}
              >
                新建
              </Button>
            </div>
          </div>
          <div className={styles.content}>
            <MyCard
              list={data}
              isAction={true}
              renderList={renderList}
            ></MyCard>
            <div className={styles.footer_box}>
              <div></div>
              <Pagination
                total={12}
                showTotal={() => `共5条`}
                defaultPageSize={12}
                defaultCurrent={1}
                showSizeChanger
              />
            </div>
          </div>
        </div>
        {/* 新建抽屉 */}
        <Drawers visible={visible} changeShowDrawer={onClose}></Drawers>
      </div>
    </div>
  );
}
