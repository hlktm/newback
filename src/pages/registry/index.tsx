import React from 'react';
import { useState } from 'react';
import { useRequest, useHistory, useModel } from 'umi';
import { Form, Input, Button, Image, message, Modal, Alert } from 'antd';
import { register } from '../../services/login';
import Registry from '../../imgs/registry.png';
import styles from './index.less';
const App: React.FC = () => {
  const history = useHistory();
  const [isModalVisible, setIsModalVisible] = useState(false);

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    history.replace('/login');
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const onFinish = async (values: any) => {
    // console.log('Success:', values);
    let res = await register(values);
    console.log(res, 'res');
    console.log(register(values));

    console.log(await register(values));

    // if (res.statusCode === 201) {
    //   showModal();
    // } else {
    //   message.error(res.msg);
    // }
    // let user =
    //   res &&
    //   res.filter((item: any, index: number) => {
    //     return item.name == values.name && item.name == values.password;
    //   })[0];
    // setInitialState(user);
    // // console.log(user);
    // // 存储用户名
    // localStorage.setItem('user', JSON.stringify(user));
    // if (user) {
    //   history.push('/');
    // } else {
    //   alert('账号或密码错误');
    // }
  };
  return (
    <div className={styles.loginAll}>
      <div className={styles.loginLeft}>
        <img src={Registry} alt="" />
      </div>
      <div className={styles.from}>
        <div className={styles.loginCon}>
          <h1>访客注册</h1>
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            labelAlign="left"
          >
            <Form.Item
              label="账户"
              name="name"
              labelCol={{ span: 3, offset: 0 }}
              wrapperCol={{ span: 20, offset: 0 }}
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="密码"
              name="password"
              labelCol={{ span: 3, offset: 0 }}
              wrapperCol={{ span: 20, offset: 0 }}
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="confirm"
              label="确认"
              labelCol={{ span: 3, offset: 0 }}
              wrapperCol={{ span: 20, offset: 0 }}
              rules={[
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error(
                        'The two passwords that you entered do not match!',
                      ),
                    );
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 0, span: 23 }}>
              <Button
                type="primary"
                htmlType="submit"
                style={{ width: '100%' }}
              >
                注册
              </Button>
            </Form.Item>
          </Form>
          <p>
            Or <a href="/login">去登录</a>
          </p>
        </div>
      </div>

      <Modal visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Alert
          description="注册成功"
          type="warning"
          showIcon
          closable={false}
        />
        <p>是否跳转登录页面</p>
      </Modal>
    </div>
  );
};

export default App;
