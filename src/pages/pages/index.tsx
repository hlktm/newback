import styles from './index.less';
import React, { useState, useEffect, useRef } from 'react';
import { useRequest } from 'umi';
import {
  Table,
  Space,
  Button,
  Input,
  Select,
  Popconfirm,
  Spin,
  message,
} from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import {
  pageList,
  DelPageList,
  PubPageList,
  DraftPageList,
  SearchPageList,
} from '../../services/pages';
import { RedoOutlined } from '@ant-design/icons';
import Breadcrumb from '@/components/breadcrumb/index';
import Access from '@/components/access/index';
import moment from 'moment';
const { Option } = Select;
interface DataType {
  key: string;
  name: string;
  path: number;
  order: number;
  views: number;
  status: string;
  publishAt: string;
  id: string;
}
export default function IndexPage() {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  // const { data, error, loading } = useRequest(pageList);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [data, setData] = useState([]);
  const [selValue, setSelValue] = useState('');
  const { run, loading } = useRequest(pageList, { manual: true });
  // console.log(data&&data[0],'pagedata');
  const delPage = useRequest(DelPageList, { manual: true });
  const PubList = useRequest(PubPageList, { manual: true });
  const DraftList = useRequest(DraftPageList, { manual: true });
  const SearchList = useRequest(SearchPageList, { manual: true });

  //第一个输入框
  const textInput = useRef(null) as any;
  //第二个输入框
  const emailInp = useRef(null) as any;
  //下拉框
  const seleInp = useRef(null) as any;

  const renderList = async () => {
    let res = await run();
    // console.log(res, 'resssss');
    setData(res[0]);
  };
  const columns: ColumnsType<DataType> = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      align: 'center',
    },
    {
      title: '路径',
      dataIndex: 'path',
      key: 'path',
      align: 'center',
    },
    {
      title: '顺序',
      dataIndex: 'order',
      key: 'order',
      align: 'center',
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'views',
      align: 'center',
      render: (text, record) => <span className={styles.numbers}>{text}</span>,
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (text) => (
        <div>
          {text === 'publish' ? (
            <div>
              <span className={styles.span_style}></span>已发布
            </div>
          ) : (
            <div>
              <span className={styles.spans_style}></span>草稿
            </div>
          )}
        </div>
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'publishAt',
      align: 'center',
      render: (text, record) => {
        return (
          <span>
            {/* {new Date(record.publishAt).toLocaleString()} */}
            {moment(record.publishAt).format('YYYY-MM-DD HH:mm:ss')}
          </span>
        );
      },
    },
    {
      title: '操作',
      key: 'action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <a href="/article/editor">编辑</a>
          {record.status === 'publish' ? (
            <a
              onClick={() => {
                PubList.run({
                  id: record.id,
                  data: { status: 'draft' },
                }).then((res) => {
                  renderList();
                });
                message.success('操作成功');
              }}
            >
              下线
            </a>
          ) : (
            <a
              onClick={() => {
                DraftList.run({
                  id: record.id,
                  data: { status: 'publish' },
                }).then((res) => {
                  renderList();
                });
                message.success('操作成功');
              }}
            >
              发布
            </a>
          )}
          <a onClick={showModal}>查看访问</a>
          <Popconfirm
            title="确定要删除吗?"
            onConfirm={() => {
              confirm(record.id);
            }}
            onCancel={cancel}
            okText="确定"
            cancelText="取消"
          >
            <a href="#">删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  useEffect(() => {
    renderList();
  }, []);
  //删除确认
  const confirm = (id: any) => {
    // console.log(e);
    // console.log(id, 'ids');
    delPage.run(id).then((res) => {
      renderList();
    });
    message.success('删除成功');
  };
  //取消删除
  const cancel = (e: any) => {
    console.log(e);
    message.error('取消删除');
  };
  const hasSelected = selectedRowKeys.length > 0;
  function onSelectChange(newSelectedRowKeys: React.Key[]) {
    // console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  }
  //全选
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  //查看访问弹框
  const showModal = () => {
    setIsModalVisible(true);
  };
  //关闭查看访问弹框
  const isModalVisibleChange = () => {
    setIsModalVisible(false);
  };
  //批量发布
  const pubSome = () => {
    selectedRowKeys.forEach((id) => {
      PubList.run({
        id,
        data: { status: 'draft' },
      }).then((res) => {
        renderList();
      });
      message.success('操作成功');
    });
  };
  //批量下线
  const draftSome = () => {
    selectedRowKeys.forEach((id) => {
      DraftList.run({
        id,
        data: { status: 'publish' },
      }).then((res) => {
        renderList();
      });
      message.success('操作成功');
    });
  };
  //批量删除
  const delSome = () => {
    selectedRowKeys.forEach((id: any) => {
      delPage.run(id).then((res) => {
        renderList();
      });
      message.success('删除成功');
    });
  };
  //搜索
  const SearchBtn = () => {
    const values = textInput.current.input.value;
    const emailValue = emailInp.current.input.value;
    SearchList.run({ name: values, path: emailValue }).then(
      (res: React.SetStateAction<never[]>[]) => {
        // console.log(res,'resssssssssssssssss')
        setData(res[0]);
      },
    );
  };
  //下拉框
  const handleChange = (value: string) => {
    setSelValue(value);
    SearchList.run({ status: selValue }).then((res) => {
      setData(res[0]);
    });
  };
  return (
    <div>
      <Breadcrumb children={{ title: '页面管理', path: '/pages' }} />
      <div className={styles.page_box}>
        <div className={styles.header_box}>
          {/* 头部搜索 */}
          <div className={styles.input_box}>
            名称:
            <Input
              placeholder="请输入页面名称"
              style={{ width: '230px' }}
              ref={textInput}
            />
            Email:
            <Input
              placeholder="请输入页面路径"
              style={{ width: '230px' }}
              ref={emailInp}
            />
            状态:
            <Select
              defaultValue=""
              style={{ width: 200 }}
              onChange={handleChange}
            >
              <Option value="draft">已发布</Option>
              <Option value="publish">草稿</Option>
            </Select>
          </div>
          <div className={styles.button_box}>
            <Button
              type="primary"
              style={{ marginRight: '30px' }}
              onClick={SearchBtn}
            >
              搜索
            </Button>
            <Button>重置</Button>
          </div>
        </div>
        {/* 内容 */}
        <div className={styles.content_box}>
          {/* 新建 */}
          <div className={styles.content_head}>
            <div>
              {' '}
              <span style={{ marginLeft: 8, width: '600px' }}>
                {hasSelected ? (
                  <>
                    {' '}
                    <Button onClick={pubSome}>发布</Button>{' '}
                    <Button onClick={draftSome}>下线</Button>{' '}
                    <Button type="primary" danger ghost onClick={delSome}>
                      删除
                    </Button>
                  </>
                ) : (
                  ''
                )}
              </span>
            </div>
            <div>
              <Button type="primary" style={{ marginRight: '30px' }}>
                <a href="/article/editor">新建</a>
              </Button>
              <RedoOutlined
                style={{ marginLeft: 14 }}
                onClick={() => {
                  renderList();
                }}
              ></RedoOutlined>
            </div>
          </div>
          {/* 表格 */}
          <Table
            rowSelection={rowSelection}
            rowKey="id"
            // style={{  paddingLeft: '10px' }}
            columns={columns}
            dataSource={data}
            pagination={{ pageSize: 5, showSizeChanger: true }}
          />
        </div>
      </div>
      <Access
        isModalVisible={isModalVisible}
        changeisModalVisible={isModalVisibleChange}
      ></Access>
    </div>
  );
}
