import styles from './index.less';
import React, { useState } from 'react';
import Breadcrumb from '../../components/breadcrumb';
import { emailList } from '../../services/email';
import { Table, Button, Input } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { ReloadOutlined } from '@ant-design/icons';
import { useRequest } from 'umi';
interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
  from: string;
  to: string;
  createAt: string;
  id: string;
}

const columns: ColumnsType<DataType> = [
  {
    title: '发件人',
    width: 100,
    dataIndex: 'from',
    key: 'from',
    fixed: 'left',
  },

  {
    title: '收件人',
    dataIndex: 'to',
    key: 'to',
    width: 150,
  },
  {
    title: '主题',
    dataIndex: 'subject',
    key: '2',
    width: 150,
  },
  {
    title: '发送时间',
    dataIndex: 'createAt',
    key: 'createAt',
    width: 150,
  },

  {
    title: '操作',
    key: 'operation',
    fixed: 'right',
    width: 100,
    align: 'center',
    render: () => <a>删除</a>,
  },
];

// const data: DataType[] = [];
// for (let i = 0; i < 100; i++) {
//   data.push({
//     key: i,
//     name: `Edrward ${i}`,
//     age: 32,
//     address: `London Park no. ${i}`,
//   });
// }
export default function IndexPage() {
  const { data, error, loading } = useRequest(emailList);
  console.log(data, 'data');
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  let datalist =
    data &&
    data[0].map((element: DataType, index: number) => {
      // console.log(element,"element");
      return { ...element, ...{ key: element.id } };
    });
  console.log(datalist, '33333');

  // const hasSelected = selectedRowKeys.length > 0;
  function onSelectChange(newSelectedRowKeys: React.Key[]) {
    // console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <div>
      <Breadcrumb children={{ title: '邮件管理', path: '/email' }} />
      <div className="head">
        <div className={styles.header_box}>
          <div className={styles.input_box}>
            文件名称:
            <Input placeholder="请输入文件名称" style={{ width: '230px' }} />
            文件类型:
            <Input placeholder="请输入文件类型" style={{ width: '230px' }} />
            主题: <Input placeholder="请输入主题" style={{ width: '230px' }} />
          </div>
          <div className={styles.button_box}>
            <Button type="primary" style={{ marginRight: '30px' }}>
              搜索
            </Button>
            <Button>重置</Button>
          </div>
          <div className={styles.ioc}>
            <ReloadOutlined />
          </div>
        </div>
      </div>
      <Table
        rowSelection={rowSelection}
        columns={columns}
        pagination={{ pageSize: 5, showSizeChanger: true }}
        dataSource={datalist}
        scroll={{ x: 1500, y: 300 }}
      />
    </div>
  );
}
