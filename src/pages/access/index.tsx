import { Table, Form, Row, Col, Input, Button, Select } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import Breadcrumb from '../../components/breadcrumb';
import RedoOutlined from '@ant-design/icons/lib/icons/RedoOutlined';
import React, { useState } from 'react';
import styles from './index.less';
interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}
const columns: ColumnsType<DataType> = [
  { title: 'URL', dataIndex: 'address', key: '1' },
  { title: 'IP', dataIndex: 'address', key: '2' },
  { title: '浏览器', dataIndex: 'address', key: '3' },
  { title: '内核', dataIndex: 'address', key: '4' },
  { title: '操作系统', dataIndex: 'address', key: '5' },
  { title: '设备', dataIndex: 'address', key: '6' },
  { title: '地址', dataIndex: 'address', key: '7' },
  { title: '访问量', dataIndex: 'address', key: '8' },
  { title: '访问时间', dataIndex: 'address', key: '9' },
  {
    title: '操作',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: () => <a>删除</a>,
  },
];
const data: DataType[] = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York Park',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 40,
    address: 'London Park',
  },
];
const rowSelection = {
  getCheckboxProps: (record: { name: string }) => ({
    disabled: record.name === 'Disabled User',
    // Column configuration not to be checked
    name: record.name,
  }),
};
const { Option } = Select;

const AdvancedSearchForm = () => {
  const [expand, setExpand] = useState(false);
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
  };

  return (
    <Form
      form={form}
      name="advanced_search"
      className={styles.souliebiao}
      onFinish={onFinish}
    >
      <div className={styles.soulis}>
        <div className={styles.sou}>
          <span>IP：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>US：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>URL：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>地址：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>浏览器：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>内核：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>OS：</span> <Input className={styles.souin} type="text" />
        </div>
        <div className={styles.sou}>
          <span>设备：</span> <Input className={styles.souin} type="text" />
        </div>
      </div>
      <div className={styles.soubutton}>
        <Button type="primary" htmlType="submit">
          搜索
        </Button>
        <Button
          style={{ margin: '0 8px' }}
          onClick={() => {
            form.resetFields();
          }}
        >
          重置
        </Button>
      </div>
    </Form>
  );
};

export default function IndexPage() {
  return (
    <div className={styles.Access_statistics}>
      <Breadcrumb children={{ title: '统计管理', path: '/access' }} />
      <AdvancedSearchForm />
      <div className={styles.mainlist}>
        <div style={{ marginBottom: 16 }} className={styles.refreshdiv}>
          <RedoOutlined className={styles.refresh} />
        </div>
        <Table
          columns={columns}
          rowSelection={{ ...rowSelection }}
          dataSource={data}
          scroll={{ x: 1300 }}
        />
        ;
      </div>
    </div>
  );
}
