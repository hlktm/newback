import styles from './index.less';
import { useRequest } from 'umi';
import { ArticleList } from '../../services/article';
import { TagList } from '../../services/tag';
import { fileList } from '../../services/file';
import { commentList } from '../../services/comments';
import { userUpdata } from '../../services/login';
import { categoryList } from '../../services/category';
import { Tabs, Avatar, Button, Checkbox, Form, Input } from 'antd';
import UploadImg from '../../components/upload/uploadImg';
import React, { useEffect, useState } from 'react';
import Locker from '../../components/locker';
const { TabPane } = Tabs;
export default function IndexPage() {
  let [isLook, setIsLook] = useState(false);
  let [data, setdata] = useState<any>('');
  let [img, setImg] = useState<any>('');
  let [username, setusername] = useState<any>(
    localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')!),
  );
  // 文章
  const article = useRequest(ArticleList).data;
  // 标签
  const tag = useRequest(TagList).data;
  // 文件
  const files = useRequest(fileList).data;
  // 评论
  const comment = useRequest(commentList).data;
  // 分类
  const category = useRequest(categoryList).data;

  const [form] = Form.useForm();
  useEffect(() => {
    localStorage.getItem('user')
      ? setdata(JSON.parse(localStorage.getItem('user')!))
      : '';
    localStorage.getItem('user')
      ? setImg(JSON.parse(localStorage.getItem('user')!))
      : '';
    setusername(data);
  }, [localStorage.getItem('user')]);
  // useEffect(()=>{

  // },[localStorage.getItem("user")])
  const { run, loading } = useRequest(userUpdata, { manual: true });

  const onChange = (key: string) => {
    // console.log(key);
  };
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };
  const setflag = () => {
    setIsLook(!isLook);
  };
  // 用户名 邮箱改变
  const onFinishFailed = async (errorInfo: any) => {
    console.log(userUpdata(errorInfo.values), 'userUpdata');
    let res = await run(errorInfo.values);
    console.log(res, 'res');
  };
  const inputValue = (value: any) => {
    console.log(value, 'value');
  };
  form.setFieldsValue(data);
  const formItemLayout = {
    labelCol: {
      xs: { span: 4 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  return (
    <div className={styles.personal}>
      <div className={styles.personLeft}>
        <p>系统概览</p>
        <ul>
          <li>累计发表了{article && article[1]}篇文章</li>
          <li>累计创建了{category && category.length}个分类</li>
          <li>累计创建了{tag && tag.length}个标签</li>
          <li>累计上传了{files && files[1]}个文件</li>
          <li>累计获得了{comment && comment[1]}个评论</li>
        </ul>
      </div>
      <div className={styles.personRight}>
        <p>个人资料</p>
        <div className={styles.tab}>
          <Tabs defaultActiveKey="1" onChange={onChange}>
            <TabPane tab="基本设置" key="1">
              <span onClick={setflag}>
                {img.avatar ? (
                  <Avatar
                    size={64}
                    style={{ margin: '10px 0' }}
                    src={img.avatar}
                  />
                ) : (
                  <Avatar size={64} style={{ margin: '10px 0' }} />
                )}
              </span>

              <Form
                name="basic"
                initialValues={username}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
              >
                <Form.Item
                  label="用户名"
                  name="name"
                  labelCol={{ span: 4 }}
                  labelAlign={'left'}
                  rules={[
                    { required: true, message: 'Please input your username!' },
                  ]}
                >
                  <Input
                    defaultValue={username && username.name}
                    onChange={() => inputValue(data.name)}
                    placeholder="请输入用户名"
                  />
                </Form.Item>

                <Form.Item
                  label="邮箱"
                  name="email"
                  labelCol={{ span: 4 }}
                  labelAlign={'left'}
                  rules={[
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please input your E-mail!',
                    },
                  ]}
                >
                  <Input
                    defaultValue={username && username.email}
                    placeholder="请输入邮箱"
                  />
                </Form.Item>

                <Form.Item wrapperCol={{ span: 3 }}>
                  <Button type="primary" htmlType="submit">
                    保存
                  </Button>
                </Form.Item>
              </Form>
            </TabPane>
            <TabPane tab="更新密码" key="2">
              <Form
                {...formItemLayout}
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={username}
                scrollToFirstError
              >
                {/* <Form.Item
                  label="用户名"
                  name="name"
                  labelCol={{ span: 4 }}
                  labelAlign={'left'}
                  rules={[
                    { required: true, message: 'Please input your username!' },
                  ]}
                >
                  <Input
                    defaultValue={username && username.name}
                    onChange={() => inputValue(data.name)} placeholder="请输入用户名" />
                </Form.Item> */}
                <Form.Item
                  name="name"
                  label="原密码"
                  labelAlign={'left'}
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!',
                    },
                  ]}
                  // hasFeedback
                >
                  <Input.Password
                    defaultValue={username && username.name}
                    placeholder="请输入旧密码"
                  />
                </Form.Item>

                <Form.Item
                  name="password"
                  label="新密码"
                  labelAlign={'left'}
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!',
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password placeholder="请输入新密码" />
                </Form.Item>

                <Form.Item
                  name="confirm"
                  label="确认密码"
                  dependencies={['password']}
                  hasFeedback
                  labelAlign={'left'}
                  rules={[
                    {
                      required: true,
                      message: 'Please confirm your password!',
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('password') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          new Error(
                            'The two passwords that you entered do not match!',
                          ),
                        );
                      },
                    }),
                  ]}
                >
                  <Input.Password placeholder="请确认新密码" />
                </Form.Item>
                <Form.Item wrapperCol={{ span: 3 }}>
                  <Button type="primary" htmlType="submit">
                    更新
                  </Button>
                </Form.Item>
              </Form>
            </TabPane>
          </Tabs>

          {isLook && <Locker />}
        </div>
      </div>
    </div>
  );
}
