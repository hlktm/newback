import styles from './index.less';
import React, { useState } from 'react';
import { useRequest } from 'umi';
import { Card, Form, Input, Button, Tag } from 'antd';
import Breadcrumb from '@/components/breadcrumb/index';
import { labelList } from '@/services/label';
export default function IndexPage() {
  const { data, error } = useRequest(labelList);
  console.log(data, 'label');

  return (
    <div>
      <Breadcrumb children={{ title: '标签管理', path: '/article/labels' }} />
      <div className={styles.labels}>
        <div className={styles.labels_left}>
          <Card title="添加标签" style={{ width: 500 }}>
            <Form>
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Please input your name',
                  },
                ]}
              >
                <Input placeholder="输入标签名称" />
              </Form.Item>
              <Form.Item name="nickname">
                <Input placeholder="输入标签值(请输入英文,作为路由使用)" />
              </Form.Item>

              <Form.Item>
                <Button type="primary">保存</Button>
              </Form.Item>
            </Form>
          </Card>
        </div>
        <div className={styles.labels_right}>
          <Card title="所有标签" style={{ width: 900 }}>
            {data &&
              data.map((item: any, index: number) => {
                return <Tag key={index}>{item.label}</Tag>;
              })}
          </Card>
        </div>
      </div>
    </div>
  );
}
