import styles from './index.less';
import React, { useState } from 'react';
import { useRequest } from 'umi';
import { Card, Form, Input, Button, Tag } from 'antd';
import Breadcrumb from '@/components/breadcrumb/index';
import { categoryList } from '@/services/classifly';
interface Props {
  ind: number;
}
export default function IndexPage() {
  const { data, error } = useRequest(categoryList);
  const [ind, setInd] = useState(0);
  const [obj, setObj] = useState({
    name: '',
    value: '',
  });
  const changeFrom = (item: any) => {
    console.log(item);
    setObj({ name: item.label, value: item.value });
  };
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };
  return (
    <div>
      <Breadcrumb
        children={{ title: '分类管理', path: '/article/sortclassify' }}
      />
      <div className={styles.classify}>
        <div className={styles.classify_left}>
          <Card title="添加分类" style={{ width: 500 }}>
            <Form onFinish={onFinish}>
              <Form.Item name="name">
                <Input placeholder="输入分类名称" />
              </Form.Item>
              <Form.Item name="value">
                <Input placeholder="输入分类值(请输入英文,作为路由使用)" />
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  保存
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </div>
        <div className={styles.classify_right}>
          <Card title="所有分类" style={{ width: 900 }}>
            {data &&
              data.map((item: any, index: number) => {
                return (
                  <Tag key={index} onClick={() => changeFrom(item)}>
                    {item.label}
                  </Tag>
                );
              })}
          </Card>
        </div>
      </div>
    </div>
  );
}
