import React from 'react';
import styles from './index.less';
import ReactDOM from 'react-dom';
import { useState, useEffect, useRef } from 'react';
import Editor from '@monaco-editor/react';
import markdown from './markdown.less';
import MarkdownIt from 'markdown-it';
import { message, Popconfirm, Button } from 'antd';
import { useHistory } from 'umi';
import { EllipsisOutlined } from '@ant-design/icons';
import { ArticleList } from '../../../services/article';
const editor = () => {
  const history = useHistory();
  const [html, sethtml] = useState('');
  const [content, setcontent] = useState('');
  const [height, setheight] = useState(0);
  const context = useRef<any>(null);
  const valueChange = (val: any) => {
    // console.log(val);
    // console.log(context.current.render(val));
    sethtml(context.current.render(val));
  };
  useEffect(() => {
    setheight(window.document.documentElement.offsetHeight);
    ArticleList({}).then((res) => {
      // console.log(res.data[0][0]);
      let data = res.data[0][0];
      sethtml(data.html);
      setcontent(data.content);
    });
    context.current = new MarkdownIt({
      html: false, // 在源码中启用 HTML 标签
      xhtmlOut: false, // 使用 '/' 来闭合单标签 （比如 <br />）。
      // 这个选项只对完全的 CommonMark 模式兼容。
      breaks: false, // 转换段落里的 '\n' 到 <br>。
      langPrefix: 'language-', // 给围栏代码块的 CSS 语言前缀。对于额外的高亮代码非常有用。
      linkify: false, // 将类似 URL 的文本自动转换为链接。

      // 启用一些语言中立的替换 + 引号美化
      typographer: false,

      // 双 + 单引号替换对，当 typographer 启用时。
      // 或者智能引号等，可以是 String 或 Array。
      //
      // 比方说，你可以支持 '«»„“' 给俄罗斯人使用， '„“‚‘'  给德国人使用。
      // 还有 ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'] 给法国人使用（包括 nbsp）。
      quotes: '“”‘’',
    });
  }, []);
  window.onresize = () => {
    setheight(window.document.documentElement.offsetHeight);
  };
  const confirm = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    message.success('Click on Yes');
    history.go(-1);
  };

  const cancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    message.error('取消关闭');
  };
  return (
    <div className={styles.editor}>
      <div className={styles.head}>
        <span>
          <Popconfirm
            title="确定关闭？如果内容有变更，请先保存"
            onConfirm={confirm}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <button>x</button>
          </Popconfirm>
          <input type="text" placeholder="请输入文章标题" />
        </span>

        <span>
          <Button type="primary">发布</Button>
          <EllipsisOutlined />
        </span>
      </div>
      <div className={styles.main}>
        <div className={styles.left}>
          <Editor
            height="90vh"
            defaultLanguage="markdown" //默认格式
            // theme='vs-dark' // 黑色背景
            value={content && content} //默认内容
            onChange={valueChange} //监听内容改变
          />
        </div>
        <div className={styles.right} style={{ height: height }}>
          <div
            className={markdown.markdown}
            dangerouslySetInnerHTML={{ __html: html }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default editor;
