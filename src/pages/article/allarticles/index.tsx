import styles from './index.less';
import { useRequest, useSelector } from 'umi';
import { useState, useEffect } from 'react';
import { Table, Input, Select, Button, Space, Tag, Badge, message } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { ArticleList, isRecommended } from '@/services/article';
import Breadcrumb from '@/components/breadcrumb/index';
import Popconfirm from '@/components/Popconfirm/index';
import Access from '@/components/access/index';
import TableComponent from '@/components/table/index';
import { DataType } from '@/utils/type';
import usePage from '@/hooks/usePage';
import useList from '@/hooks/useList';
const { Option } = Select;
const IndexPage = (props: any) => {
  const [pageSize, setpageSize] = useState(5);
  const [page, setpage] = useState(1);
  const [data, setdata] = useState([]);
  // const { data, error } = useRequest(() =>
  //   ArticleList({
  //     pageSize,
  //     page,
  //   }),
  // );

  const [isModalVisible, setIsModalVisible] = useState(false);
  // 首焦
  const isRecommended1 = useRequest(isRecommended, { manual: true });
  const state = useSelector((state: any) => state.article);
  console.log(state, 'state');
  const { run, loading } = useRequest(ArticleList, { manual: true });
  const getList = async () => {
    let res = await run({
      page,
      pageSize,
    });
    setdata(res[0]);
  };
  useEffect(() => {
    getList();
  }, []);
  //分页
  const pagination = usePage({
    total: 17,
    current: 1,
    pageSize: 5,
    onChange: (current: number, pageSize: number) => {
      //发请求
      console.log(444);
      ArticleList({ page: current, pageSize: pageSize });
    },
  }); //返回一个对象
  // useEffect(() => {
  //   ArticleList({ page, pageSize });
  // }, [page]);

  const isModalVisibleChange = () => {
    setIsModalVisible(false);
  };
  // 首焦
  const changeRecommended = (item: any) => {
    isRecommended1.run({ id: item.id, isRecommended: !item.isRecommended });
    message.success('操作成功');
    getList();
  };

  //表头
  const columns: ColumnsType<DataType> = [
    {
      title: '标题',
      width: 200,
      fixed: 'left',
      dataIndex: 'title',
      key: '1',
      align: 'center',
    },
    {
      title: '状态',
      width: 100,
      dataIndex: 'status',
      key: '2',
      align: 'center',
      render: (text, record) => {
        // console.log(text, 'text');
        return (
          <>
            {text === 'publish' ? (
              <Badge
                overflowCount={0}
                className="site-badge-count-109"
                dot={true}
                offset={[-50, 6]}
                style={{ backgroundColor: '#52c41a' }}
              >
                已发布
              </Badge>
            ) : (
              <Badge
                overflowCount={0}
                className="site-badge-count-109"
                dot={true}
                offset={[-50, 6]}
                style={{ backgroundColor: 'gold' }}
              >
                草稿
              </Badge>
            )}
          </>
        );
      },
    },
    {
      title: '分类',
      dataIndex: 'category',
      key: '3',
      width: 150,
      align: 'center',
      render: (text, record) => {
        return (
          <Tag
            color={
              record.category && record.category.label === '阅读'
                ? '#cd201f'
                : record.category && record.category.label === '前端'
                ? 'gold'
                : record.category && record.category.label === 'LeetCode'
                ? '#3b5999'
                : 'gold'
            }
          >
            {record.category && record.category.label}
          </Tag>
        );
      },
    },
    {
      title: '标签',
      dataIndex: 'tags',
      key: '4',
      width: 150,
      align: 'center',
      render: (text, record) => {
        return (
          <div>
            {record.tags &&
              record.tags.map((item: any, index: number) => {
                return (
                  <Tag
                    color={
                      record.tags && record.tags === '阅读'
                        ? '#cd201f'
                        : record.tags && record.tags === '前端'
                        ? '#55acee'
                        : record.tags && record.tags === 'LeetCode'
                        ? '#3b5999'
                        : '#3b5999'
                    }
                    key={index}
                  >
                    {item.label}
                  </Tag>
                );
              })}
          </div>
        );
      },
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: '5',
      width: 150,
      align: 'center',
      render: (text, record) => (
        <Badge
          overflowCount={1000000000000}
          className="site-badge-count-109"
          count={record.views}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      key: '6',
      width: 150,
      align: 'center',
      render: (text, record) => {
        return (
          <Badge
            className="site-badge-count-109"
            showZero
            overflowCount={1000}
            count={record.likes}
            style={{ backgroundColor: 'magenta' }}
          />
        );
      },
    },

    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: '7',
      width: 150,
      align: 'center',
      render: (text, record) => {
        return <span>{new Date(record.publishAt).toLocaleString()}</span>;
      },
    },
    {
      title: '操作',
      key: 'action',
      width: 260,
      fixed: 'right',
      // align:"center",
      render: (text, record) => {
        // console.log(record.isRecommended, "00");
        return (
          <Space size="middle">
            <a>编辑</a>
            {record.isRecommended === true ? (
              <a onClick={() => changeRecommended(record)}>首焦推荐</a>
            ) : (
              <a onClick={() => changeRecommended(record)}>撤销首焦</a>
            )}

            <a onClick={showModal}>查看访问</a>
            <Popconfirm data={record} getList={getList}>
              <a href="#" onClick={() => {}}>
                删除
              </a>
            </Popconfirm>
          </Space>
        );
      },
    },
  ];
  //弹框
  const showModal = () => {
    setIsModalVisible(true);
  };
  return (
    <div>
      <Breadcrumb
        children={{ title: '所有文章', path: '/article/allarticles' }}
      />
      <div className={styles.articles_head}>
        {/* 搜索 */}
        <div className={styles.articles_input}>
          <p>
            {' '}
            标题 :{' '}
            <Input placeholder="请输入文章标题" style={{ width: '276px' }} />
          </p>
          <p>
            状态 :{' '}
            <Select defaultValue="lucy" style={{ width: 120 }} allowClear>
              <Option value="已发布">已发布</Option>
              <Option value="草稿">草稿</Option>
            </Select>
          </p>
          <p>
            分类 :{' '}
            <Select defaultValue="lucy" style={{ width: 120 }} allowClear>
              <Option value="lucy">Lucy</Option>
            </Select>
          </p>
          <p></p>
          <p></p>
        </div>
        <div className={styles.articles_btn}>
          <Button type="primary">搜索</Button> <Button>重置</Button>
        </div>
      </div>
      <div className={styles.articles_main}>
        {/* 表格 */}
        <div className={styles.articles_table}>
          <TableComponent
            typeList="article"
            head={columns}
            data1={data}
            isAllSelect={true}
            getData={getList}
            baseProps={{
              rowKey: 'id',
              scroll: { x: '1500' },
              pagination,
            }}
          />
        </div>
        <Access
          isModalVisible={isModalVisible}
          changeisModalVisible={isModalVisibleChange}
        ></Access>
      </div>
    </div>
  );
};
export default IndexPage;
