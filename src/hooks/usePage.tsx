import { useState, useEffect, useMemo } from 'react';
import { PageType } from '@/utils/type';
const defaultPagination = {
  current: 1,
  pageSize: 5,
  total: 0,
  // onChange:(page:number,pageSize:number)=>{}
};
const usePage = (config: PageType = defaultPagination) => {
  console.log(config, 'config');
  console.log(defaultPagination);
  const [Pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });
  // 自定义hook ，以use开头，在自定义hook的内部，可以使用hook钩子函数
  const pagination = useMemo(() => {
    return {
      ...defaultPagination,
      ...config,
      current: Pagination.current,
      pageSize: Pagination.pageSize,
      onChange: (current: number, pageSize: number) => {
        // 点击页码数的时候那更改一个事件
        if (config.onChange) {
          config.onChange(current, pageSize);
        }
        setPagination({ current, pageSize });
      },
    };
  }, [config, Pagination]);

  return pagination;
};
export default usePage;
