import { useRequest } from 'umi';
import { useState, useEffect } from 'react';
import usePage from './usePage';
const useList = (type: any) => {
  console.log(type);

  const [data, setData] = useState({
    list: [],
    total: 0,
  });
  const { run, loading } = useRequest(type, { manual: true });
  const getinit = async (info: any) => {
    let res = await run(info);
    console.log(res, 'res');

    setData({
      list: res[0],
      total: res[1],
    });
    useEffect(() => {
      getinit({ page: 1, pageSize: 5 });
    }, []);
  };
  const pagination = usePage({
    total: data.total,
    current: 1,
    pageSize: 5,
    onChange: (current: number, pageSize: number) => {
      getinit({ page: current, pageSize });
    },
  });

  return {
    list: data.list,
    total: data.total,
    pagination,
  };
};
export default useList;
