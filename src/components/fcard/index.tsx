import React, { Component } from 'react';
import { Card } from 'antd';
import styles from './index.less';
const { Meta } = Card;
export default function Index(props: any) {
  console.log(props, 'props');
  const { list } = props;
  return (
    <div className={styles.card}>
      {list &&
        list.map((item: any, index: number) => {
          return (
            <Card
              hoverable
              key={index}
              style={{ width: '16%', marginTop: 10 }}
              cover={
                <img
                  alt="example"
                  src={item.cover || item.url}
                  style={{ width: '100%', height: '150px' }}
                />
              }
              onClick={() => {
                props.onShow();
              }}
            >
              <Meta
                title={item.title || item.originalname}
                description={`上传于` + item.createAt}
              />
            </Card>
          );
        })}
    </div>
  );
}
