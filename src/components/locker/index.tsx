import styles from './index.less';
import { InboxOutlined } from '@ant-design/icons';
import React, { useEffect, useState } from 'react';
import { fileList } from '@/services/file';
import MyCard from '@/components/card/index';
import { Space, Button, Form, Input, Switch, Upload, Card, Drawer } from 'antd';
import Pages from '../pagination';
// 个人中心抽屉
export default function locker(props: any) {
  let [page, setpage] = useState(1);
  let [pageSize, setPageSize] = useState(12);
  let [total, setTotal] = useState(50);
  let [isLook, setIsLook] = useState(true);
  let [fileData, setFileData] = useState<any>([[], 0]);

  useEffect(() => {
    fileList({ page, pageSize }).then((res: any) => {
      console.log(res, 'res');
      setFileData(res.data);
      setTotal(res.data[1]);
    });
  }, [page, pageSize]);
  console.log(fileData, 'fileData');
  // // 抽屉
  const [childrenDrawer, setChildrenDrawer] = useState(true);
  // // console.log(data && data[0], 'daa');
  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 22 },
  };
  //打开抽屉
  const showChildrenDrawer = () => {
    setChildrenDrawer(true);
  };
  //关闭抽屉
  const onChildrenDrawerClose = () => {
    setChildrenDrawer(false);
  };

  const clinkCard = (item: any) => {
    console.log(item, 'item');
    // localStorage.getItem("user") && localStorage.setItem("user", JSON.stringify(JSON.parse(localStorage.getItem("user")!).avatar = item.url))
    localStorage.getItem('user') &&
      localStorage.setItem(
        'user',
        JSON.stringify({
          ...JSON.parse(localStorage.getItem('user')!),
          ...{ avatar: item.url },
        }),
      );
    setChildrenDrawer(false);
  };
  const handPage = (page: number, pageSize: any) => {
    console.log(page, pageSize, 'pageSize');
    setpage(page);
    setPageSize(pageSize);
    // getFile({ page, pageSize })
  };
  return (
    <div>
      {/* 二级抽屉 */}
      <Drawer
        title="文件选择"
        width={920}
        closable={false}
        onClose={onChildrenDrawerClose}
        visible={childrenDrawer}
        extra={
          <Space>
            <Button onClick={onChildrenDrawerClose} style={{ border: 'none' }}>
              X {page}
            </Button>
          </Space>
        }
      >
        <div className={styles.header_box}>
          <div className={styles.input_box}>
            文件名:
            <Input placeholder="请输入文件名" style={{ width: '190px' }} />
            文件类型:
            <Input placeholder="请输入文件类型" style={{ width: '190px' }} />
          </div>
          <div className={styles.buttons_box}>
            <Button type="primary" style={{ marginRight: '30px' }}>
              搜索
            </Button>
            <Button>重置</Button>
          </div>
        </div>
        <div className={styles.content_drawer}>
          <div className={styles.content_drawer_top}>
            <div></div>
            <div>
              {' '}
              <Upload>
                <Button>上传文件</Button>
              </Upload>
            </div>
          </div>
          <div className={styles.content_drawer_con}>
            {fileData &&
              fileData.length &&
              fileData[0].map((item: any, index: number) => {
                return (
                  <Card
                    className={styles.cards}
                    key={index}
                    hoverable={true}
                    onClick={() => clinkCard(item)}
                  >
                    <img
                      src={item.url}
                      alt=""
                      style={{ width: '100%', height: '110px' }}
                    />
                    <h3>{item.originalname}</h3>
                  </Card>
                );
              })}
          </div>
          <Pages
            page={page} //当前页
            pageSize={pageSize} //每页条数
            total={total} //  总条数
            isLook={isLook} //  是否展示
            changePage={handPage}
            looktotal={true} //是否展示总条数
          />
        </div>
      </Drawer>
    </div>
  );
}
