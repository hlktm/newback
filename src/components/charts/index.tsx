import React from 'react';
import * as charts from 'echarts';
import ReactEcharts from 'echarts-for-react';

type EChartsOption = charts.EChartsOption;
const getOption = () => {
  const option: EChartsOption = {
    // 图表标题
    title: {
      text: '每周用户访问指标',
    },
    // （提示框组件相关的行为，必须引入提示框组件后才能使用）
    tooltip: {
      trigger: 'axis', // 触发类型，默认数据触发，见下图，可选为：'item' ¦ 'axis'
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: 'cross', // 默认为直线，可选为：'line' | 'shadow'
      },
      showDelay: 20,
      hideDelay: 1, // 隐藏延迟，单位ms
      backgroundColor: 'rgba(0,0,0,0.7)', // 提示背景颜色，默认为透明度为0.7的黑色
      borderColor: '#333', // 提示边框颜色
      borderRadius: 4, // 提示边框圆角，单位px，默认为4
      borderWidth: 0, // 提示边框线宽，单位px，默认为0（无边框）
      padding: 5, // 提示内边距，单位px，默认各方向内边距为5，
      // 接受数组分别设定上右下左边距，同css
      textStyle: {
        //提示框的文本颜色
        color: '#fff',
      },
    },
    // 图例 （图例组件相关的行为，必须引入图例组件后才能使用）
    legend: {
      data: ['评论数', '访问量'],
    },
    // 网格
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    },
    yAxis: {},
    series: [
      {
        name: '评论数',
        type: 'bar',
        data: [110, 200, 150, 90, 80, 110, 120],
        color: ['#c23631'],
        emphasis: {
          focus: 'series',
        },
        animationDelay: function (idx) {
          return idx * 10;
        },
      },
      {
        name: '访问量',
        type: 'line',
        data: [360, 290, 350, 110, 100, 120, 160],
        color: ['#304655'],
        emphasis: {
          focus: 'series',
        },
        animationDelay: function (idx) {
          return idx * 10 + 100;
        },
      },
    ],
    animationEasing: 'elasticOut',
    animationDelayUpdate: function (idx) {
      return idx * 5;
    },
  };
  return option;
};

export default function Echarts() {
  return (
    <div style={{ marginTop: '15px', lineHeight: '2' }}>
      <ReactEcharts option={getOption()}></ReactEcharts>
    </div>
  );
}
