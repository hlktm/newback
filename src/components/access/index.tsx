import { Button, Modal } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './index.less';
import { Slider } from 'antd';
const App: React.FC<any> = (props: any) => {
  // console.log(props, 'prospsss');

  const handleOk = () => {
    props.changeisModalVisible();
  };

  // useEffect(()=>{
  // console.log(props);
  // },[])
  return (
    <>
      <Modal
        title="访问统计"
        visible={props.isModalVisible}
        onOk={handleOk}
        onCancel={handleOk}
      >
        <div className={styles.earch}>
          <div className={styles.slider}>
            {' '}
            <Slider range={{ draggableTrack: true }} defaultValue={[20, 50]} />
          </div>
        </div>
      </Modal>
    </>
  );
};

export default App;
