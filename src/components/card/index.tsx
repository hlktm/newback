import styles from './index.less';
import React, { useEffect, useState } from 'react';
import { useRequest } from 'umi';
import { DelKnowList } from '@/services/knowledge';
import {
  EditOutlined,
  DeleteOutlined,
  SettingOutlined,
  CloudDownloadOutlined,
} from '@ant-design/icons';
import MyDrawer from '@/components/drawer';
import { Card, message, Popconfirm, Tooltip } from 'antd';
const { Meta } = Card;

export default function IndexPage(props: any) {
  const [visible, setVisible] = useState(false);
  const delKnow = useRequest(DelKnowList, { manual: true });
  //删除确认
  const confirm = (id: any) => {
    // console.log(e);
    delKnow.run(id).then((_res) => {
      props.renderList();
    });
    message.success('删除成功');
  };
  //取消删除
  const cancel = (e: any) => {
    console.log(e);
    message.error('取消删除');
  };
  //打开抽屉
  const showDrawer = () => {
    setVisible(true);
  };
  //关闭抽屉
  const onClose = () => {
    setVisible(false);
  };
  // console.log(props, 'props');
  const { list } = props;
  return (
    <div className={styles.card_style}>
      {list &&
        list.map((item: any, index: number) => {
          return (
            <Card
              style={{ width: '23%', marginTop: 10, marginLeft: 15 }}
              // hoverable={true}
              key={index}
              cover={
                <img
                  alt="example"
                  src={item.cover || item.url}
                  style={{ width: '100%', height: '150px' }}
                />
              }
              actions={[
                <EditOutlined
                  key="edit"
                  onClick={() => {
                    console.log('');
                  }}
                />,
                <>
                  <Tooltip title="设为草稿">
                    <CloudDownloadOutlined />,
                  </Tooltip>
                </>,
                <SettingOutlined key="setting" onClick={showDrawer} />,
                <Popconfirm
                  title="确定要删除吗?"
                  onConfirm={() => {
                    confirm(item.id);
                  }}
                  onCancel={cancel}
                  okText="确定"
                  cancelText="取消"
                >
                  <DeleteOutlined />
                </Popconfirm>,
              ]}
            >
              <Meta
                style={{ height: 65 }}
                title={item.title || item.originalname}
                description={item.summary}
              />
            </Card>
          );
        })}
      <MyDrawer visible={visible} changeShowDrawer={onClose}></MyDrawer>
    </div>
  );
}
