import React from 'react';
import { Pagination } from 'antd';

function pagination(props: any) {
  // console.log(props,"props");

  let { current, total, pageSize, isLook, changePage, looktotal } = props;
  const handleChange = (page: any, pageSize: any) => {
    // console.log(page, pageSize,"val");
    changePage(page, pageSize);
  };
  return isLook ? (
    <Pagination
      showTotal={looktotal ? (total) => `总共${total}条数据` : (total) => null}
      defaultCurrent={current}
      total={total}
      pageSize={pageSize}
      onChange={handleChange}
    />
  ) : (
    <></>
  );
}

export default pagination;
