import styles from './index.less';
import { Menu, Dropdown, Space, Button, Popover } from 'antd';
import { DownOutlined, SmileOutlined } from '@ant-design/icons';

const menu = (
  <Menu
    onClick={(e) => {
      e.domEvent.stopPropagation();
    }}
    items={[
      {
        key: '1',
        label: <a href="/personal">新建文章-协同编辑器</a>,
      },
      {
        key: '2',
        label: <a href="/article/editor"> 新建文章</a>,
      },
      {
        key: '3',
        label: <a href="/article/editor"> 新建页面</a>,
      },
    ]}
  />
);
const content = (
  <div>
    <p>
      {' '}
      <a href="/personal">新建文章-协同编辑器</a>
    </p>
    <p>
      <a href="/editor"> 新建文章</a>
    </p>
    <p>
      <a href="/editor"> 新建文章</a>
    </p>
  </div>
);

export default function IndexPage() {
  return (
    <div className={styles.menuHeader}>
      <div className={styles.menuImg}>
        <img
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png"
          alt=""
        />
        <h2>后台管理</h2>
      </div>
      <div className={styles.menuBtn}>
        <Dropdown overlay={menu}>
          <Space>
            <Button type="primary" block>
              + 新建
            </Button>
            <DownOutlined />
          </Space>
        </Dropdown>
        {/* <Popover placement="bottom"  content={content} trigger="click">
          <Button type="primary" block>
            + 新建
          </Button>
        </Popover> */}
      </div>
    </div>
  );
}
