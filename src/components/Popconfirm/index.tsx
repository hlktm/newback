import React from 'react';
import { message, Popconfirm } from 'antd';
import { useRequest } from 'umi';
import { delList, ArticleList } from '@/services/article';
interface Type {
  children: any;
  data: any;
}
export default function index(props: any) {
  // 删除
  const delData = useRequest(delList, { manual: true });
  const confirm = (e: any) => {
    console.log(e);
    console.log(props, '0000');
    delData.run(props.data).then(() => {
      props.getList();
    });
    message.success('删除成功');
  };

  const cancel = (e: any) => {
    // console.log(e);
    // message.error('访客无权进行该操作');
  };
  return (
    <div>
      {' '}
      <Popconfirm
        title="确认删除这个文章"
        onConfirm={confirm}
        onCancel={cancel}
        okText="确认"
        cancelText="取消"
      >
        <a href="#">删除</a>
      </Popconfirm>
    </div>
  );
}
