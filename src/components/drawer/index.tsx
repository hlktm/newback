import styles from './index.less';
import { InboxOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import { useRequest } from 'umi';
import { fileList } from '@/services/file';
import MyCard from '@/components/card/index';
import { Space, Button, Form, Input, Switch, Upload, Card, Drawer } from 'antd';
export default function IndexPage(props: any) {
  const fileData = useRequest(fileList).data;
  // console.log(fileData, 'fileData');
  // // const { data, error, loading } = useRequest(fileList)
  // //子抽屉
  const [childrenDrawer, setChildrenDrawer] = useState(false);
  // // console.log(data && data[0], 'daa');
  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 22 },
  };
  // //打开抽屉
  // const showDrawer = () => {
  //   setVisible(true);
  // };
  //关闭抽屉
  const onClose = () => {
    props.changeShowDrawer();
  };
  const onFinish = (values: any) => {
    console.log(values);
  };
  //打开二级抽屉
  const showChildrenDrawer = () => {
    setChildrenDrawer(true);
  };
  //关闭二级抽屉
  const onChildrenDrawerClose = () => {
    setChildrenDrawer(false);
  };
  return (
    <div>
      {/* 新建抽屉 */}
      <Drawer
        title="新建知识知识库"
        width={650}
        visible={props.visible}
        bodyStyle={{ paddingBottom: 80 }}
        onClose={onClose}
        closable={false}
        extra={
          <Space>
            <Button onClick={onClose} style={{ border: 'none' }}>
              X
            </Button>
          </Space>
        }
      >
        <Form {...layout} name="nest-messages" onFinish={onFinish}>
          <Form.Item name={['user', 'name']} label="名称">
            <Input />
          </Form.Item>
          <Form.Item name={['user', 'introduction']} label="描述">
            <Input.TextArea />
          </Form.Item>
          <Form.Item label="评论" valuePropName="checked">
            <Switch />
          </Form.Item>
          <Form.Item label="封面">
            <Form.Item
              name="dragger"
              valuePropName="fileList"
              // getValueFromEvent={normFile}
              noStyle
            >
              <Upload.Dragger name="files" action="/upload.do">
                <p className="ant-upload-drag-icon">
                  <InboxOutlined />
                </p>
                <p className="ant-upload-text">
                  点击选择文件或将文件拖拽到此处
                </p>
                <p className="ant-upload-hint">
                  文件将上传到OSS,如未配置请先配置
                </p>
              </Upload.Dragger>
            </Form.Item>
          </Form.Item>
          <Form.Item
            name={['user', 'name']}
            wrapperCol={{ ...layout.wrapperCol, offset: 2 }}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={['user', 'name']}
            wrapperCol={{ ...layout.wrapperCol, offset: 2 }}
          >
            <Button onClick={showChildrenDrawer}>选择文件</Button>
            {/* 二级抽屉 */}
            <Drawer
              title="文件选择"
              width={920}
              closable={false}
              onClose={onChildrenDrawerClose}
              visible={childrenDrawer}
              extra={
                <Space>
                  <Button
                    onClick={onChildrenDrawerClose}
                    style={{ border: 'none' }}
                  >
                    X
                  </Button>
                </Space>
              }
            >
              <div className={styles.header_box}>
                <div className={styles.input_box}>
                  文件名:
                  <Input
                    placeholder="请输入文件名"
                    style={{ width: '190px' }}
                  />
                  文件类型:
                  <Input
                    placeholder="请输入文件类型"
                    style={{ width: '190px' }}
                  />
                </div>
                <div className={styles.buttons_box}>
                  <Button type="primary" style={{ marginRight: '30px' }}>
                    搜索
                  </Button>
                  <Button>重置</Button>
                </div>
              </div>
              <div className={styles.content_drawer}>
                <div className={styles.content_drawer_top}>
                  <div></div>
                  <div>
                    {' '}
                    <Upload>
                      <Button>上传文件</Button>
                    </Upload>
                  </div>
                </div>
                <div className={styles.content_drawer_con}>
                  {fileData &&
                    fileData[0].map((item: any, index: number) => {
                      return (
                        <Card
                          className={styles.cards}
                          key={index}
                          hoverable={true}
                        >
                          <img
                            src={item.url}
                            alt=""
                            style={{ width: '100%', height: '110px' }}
                          />
                          <h3>{item.originalname}</h3>
                        </Card>
                      );
                    })}
                  {/* <MyCard list={fileData && fileData[0]} hoverable={true}/> */}
                </div>
              </div>
            </Drawer>
          </Form.Item>
          <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 15 }}>
            <Button style={{ marginRight: 10 }} value="larger">
              取消
            </Button>
            <Button disabled value="larger">
              创建
            </Button>
          </Form.Item>
        </Form>
      </Drawer>
    </div>
  );
}
