import React, { Component, useState } from 'react';
// import type { DrawerProps, RadioChangeEvent } from 'antd';
import { Button, Drawer, Space } from 'antd';
import { useRequest } from 'umi';
import Card from '@/components/fcard/index';
import { fileList } from '@/services/file';
export default function Index(props: any) {
  const [visible, setVisible] = useState(false);
  console.log(props, '9999');

  //打开
  // const showDrawer = () => {
  //     setVisible(true);
  //   };
  //关闭
  const onClose = () => {
    props.changeShowDrawer();
  };

  //   const onChange = (e: RadioChangeEvent) => {
  //     setPlacement(e.target.value);
  //   };

  return (
    <div>
      <Drawer
        title="文件信息"
        placement="right"
        width={650}
        style={{ textAlign: 'center' }}
        bodyStyle={{ paddingBottom: 80 }}
        onClose={onClose}
        visible={props.visible}
        extra={
          <Space>
            <Button onClick={onClose} style={{ border: 'none' }}>
              X
            </Button>
          </Space>
        }
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Drawer>
    </div>
  );
}
