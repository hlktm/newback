import React from 'react';
import { Table } from 'antd';
import { dvaItemType } from '../../types/work';
export interface tabelPropsType {
  balbe?: any;
  columns?: Array<any>;
  datalist?: Array<dvaItemType>;
  pagination?: any;
  showHeader?: boolean;
}
function tableTow(props: any) {
  // console.log(props, 'props');
  let { balbe, columns, dataSource, showHeader, pagination } = props;
  // console.log(balbe);

  return (
    <Table
      rowKey={balbe.rowKey}
      columns={columns}
      dataSource={dataSource}
      showHeader={showHeader}
      pagination={pagination}

      //   columns={columns}

      //     dataSource={datalist}
      //     showHeader={false}
      //     pagination={false}
    />
  );
}

export default tableTow;
