import React from 'react';
import { Table, Input, Select, Button, Space, Tag, Badge, message } from 'antd';
import { DataType } from '@/utils/type';
import { useState, useEffect } from 'react';
import styles from './index.less';
import type { ColumnsType, TablePaginationConfig } from 'antd/lib/table';
import { DatabaseOutlined, RedoOutlined } from '@ant-design/icons';
import {
  publishData,
  isRecommended,
  draftData,
  delList,
} from '@/services/article';
import { useRequest } from 'umi';
import { Tooltip } from 'antd';
interface props {
  head?: ColumnsType<DataType>;
  isAllSelect?: boolean;
  data1?: DataType[];
  baseProps?: {
    rowKey?: string;
    scroll?: { x: string };
    pagination: {};
    [propName: string]: any; //任意属性
  };
  getData?: () => any;
  typeList: string;
}
interface Types {
  text?: string;
  type?: string;
}
interface typeList {
  article?: Types[];
  page: Types[];
}
const TableComponent = (props: any) => {
  const [flag, setFlag] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  // const { run, loading } = useRequest({ manual: true })

  const { head, data1, isAllSelect, baseProps, typeList } = props;
  console.log(props, 'props');

  console.log(baseProps, 'baseProps');
  // 首焦
  const isRecommended1 = useRequest(isRecommended, { manual: true });
  // 发布
  const publishData1 = useRequest(publishData, { manual: true });
  // 草稿 draftData
  const draftData1 = useRequest(draftData, { manual: true });
  // 删除 delList
  const delList1 = useRequest(delList, { manual: true });
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    newSelectedRowKeys.length > 0 ? setFlag(true) : setFlag(false);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const btnList: typeList = {
    article: [
      {
        type: 'publish',
        text: '发布',
      },
      {
        type: 'draft',
        text: '草稿',
      },
      {
        type: 'recommend',
        text: '首焦推荐',
      },
      {
        type: 'undo',
        text: '撤销推荐',
      },
      {
        type: 'del',
        text: '删除',
      },
    ],
    page: [
      {
        type: 'publish',
        text: '发布',
      },
      {
        type: 'outline',
        text: '下线',
      },
      {
        type: 'del',
        text: '删除',
      },
    ],
  };
  return (
    <div>
      <div className={styles.table_btn}>
        {flag && (
          <div className={styles.table_btn1}>
            {btnList[typeList] &&
              btnList[typeList].map((item: any, index: number) => {
                return (
                  <Button
                    key={index}
                    onClick={() => {
                      if (item.type === 'recommend') {
                        selectedRowKeys.forEach((ite: any, index) => {
                          // console.log(ite, "555");
                          console.log(
                            isRecommended({ id: ite, isRecommended: true }),
                          ),
                            'isRecommended1';
                          isRecommended({ id: ite, isRecommended: false }).then(
                            (res) => {
                              console.log(res, 'res');
                              props.getData();
                            },
                          );
                        });

                        message.success('操作成功');
                      } else if (item.type === 'undo') {
                        selectedRowKeys.forEach((ite: any, index) => {
                          console.log(ite, '555');
                          isRecommended1
                            .run({ id: ite, isRecommended: true })
                            .then(() => {
                              props.getData();
                            });
                        });

                        message.success('操作成功');
                      } else if (item.type === 'publish') {
                        selectedRowKeys.forEach((ite: any, index) => {
                          console.log(ite, '555');

                          publishData1
                            .run({ id: ite, status: 'publish' })
                            .then(() => {
                              props.getData();
                            });
                        });

                        message.success('操作成功');
                      } else if (item.type === 'draft') {
                        selectedRowKeys.forEach((ite: any, index) => {
                          console.log(ite, '555');
                          draftData1
                            .run({ id: ite, status: 'draft' })
                            .then(() => {
                              props.getData();
                            });
                        });
                        message.success('操作成功');
                      } else if (item.type === 'del') {
                        selectedRowKeys.forEach((ite: any, index) => {
                          delList1.run(ite).then(() => {
                            props.getData();
                          });
                        });
                        message.success('操作成功');
                      }
                    }}
                  >
                    {item.text}
                  </Button>
                );
              })}
          </div>
        )}

        <div style={{ width: '700px' }}></div>

        <div style={{ marginRight: 30 }} className={styles.table_add}>
          {' '}
          <Button type="primary">新建+</Button>
          <Tooltip title="刷新">
            <RedoOutlined
              style={{ marginLeft: 20 }}
              onClick={() => {
                props.getData();
              }}
            />
          </Tooltip>
        </div>
      </div>

      <div className={styles.table_table}>
        <Table
          rowKey="id"
          columns={head}
          rowSelection={isAllSelect ? rowSelection : undefined}
          {...baseProps}
          dataSource={data1}
        />
      </div>
    </div>
  );
};
export default TableComponent;
