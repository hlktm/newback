import styles from './index.less';
import { Menu, Dropdown, Space, Avatar, message, Image } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { GitHref, gitCode } from '../../utils/gitee';
import { DownOutlined, GithubOutlined } from '@ant-design/icons';
import { useEffect, useState } from 'react';

const menu = (
  <Menu
    items={[
      {
        key: '1',
        label: <a href="/personal">个人中心</a>,
      },
      {
        key: '2',
        label: <a href="/user">用户管理 </a>,
      },
      {
        key: '3',
        label: <a href="/system">系统设置</a>,
      },
      {
        key: '4',
        label: <a href="/login">退出登录</a>,
      },
    ]}
  />
);
export default function IndexPage() {
  let [flag, getFlag] = useState(false);
  let [img, setImg] = useState('');
  let user =
    localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')!);
  // console.log(user, 'user');
  useEffect(() => {
    gitCode();
    if (gitCode() === 'code') {
      message.warning('第三方登录成功');
    } else if (gitCode() === 'error') {
      message.warning('第三方登录失败');
    } else {
    }
  }, [flag]);
  useEffect(() => {
    let imgSrc =
      localStorage.getItem('user') &&
      JSON.parse(localStorage.getItem('user')!).avatar;
    console.log(imgSrc, 'imgser');
    setImg(imgSrc);
  }, []);

  return (
    <div className={styles._right}>
      <a
        className={styles._283jLEQ1DhWAe2fw}
        onClick={() => {
          GitHref();
          getFlag(!flag);
        }}
        target="_blank"
        rel="noreferrer"
      >
        <GithubOutlined />
      </a>
      <div className={styles.right_icon}>
        {/* {
          localStorage.getItem("user") ? <Avatar  src={
            JSON.parse(localStorage.getItem("user")!).avatar ? JSON.parse(localStorage.getItem("user")!).avatar:""
          } style={{ width: 32 }}  /> :""
        } */}
        <Avatar src={img} style={{ width: 32 }} />
      </div>
      <div className={styles.right_user}>
        <Dropdown overlay={menu}>
          <Space>
            <b>Hi,{user && user.name}</b>
            <DownOutlined />
          </Space>
        </Dropdown>
      </div>
    </div>
  );
}
