import styles from './index.less';
import {
  EditOutlined,
  DeleteOutlined,
  SettingOutlined,
  CloudDownloadOutlined,
} from '@ant-design/icons';
import { Card } from 'antd';
import moment from 'moment';
const { Meta } = Card;
// interface Iprops{
//   list:
// }
export default function IndexPage(props: any) {
  console.log(props, 'props');
  const { list } = props;
  return (
    <div className={styles.card_style}>
      {list &&
        list[0].map((item: any, index: number) => {
          return (
            <Card
              style={{ width: '23%', marginTop: 10 }}
              // hoverable={true}
              key={index}
              cover={
                <img
                  alt="example"
                  src={item.cover || item.url}
                  style={{ width: '100%', height: '150px' }}
                />
              }
            >
              <Meta
                title={item.originalname}
                description={`上传于${moment(item.createAt).format(
                  'YYYY-MM-DD hh:mm:ss',
                )}`}
              />
            </Card>
          );
        })}
    </div>
  );
}
