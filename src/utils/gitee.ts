export const client = {
  id: 'd71710239707e01f2e8228e86a4c203bc5c1e1460e95391733198516d893454c',
  secret: '337c4dc3361649b61024d571221998464645d621f96f04999d57a488eec18e6f',
  uri: 'http://localhost:8000/',
};

export const GitHref = () => {
  window.location.href = `https://gitee.com/oauth/authorize?client_id=${client.id}&redirect_uri=${client.uri}&response_type=code`;
};

export const gitCode = () => {
  if (
    window.location.search.includes('?') &&
    window.location.search.includes('=')
  ) {
    //  console.log(window.location.search.split("?")[1].split("=")[0]);
    return window.location.search.split('?')[1].split('=')[0];
  }
};
