export interface DataType {
  label?: string;
  key?: React.Key;
  likes?: string;
  status?: string;
  views?: string;
  tags?: any;
  category?: any;
  id?: string;
  publishAt?: any;
  isRecommended: boolean;
}
export interface PageType {
  total?: number;
  current?: number;
  pageSize?: number;
  onChange?: (current: number, pageSize: number) => void;
}
