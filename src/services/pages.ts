import { request } from 'umi';
interface Params {
  [propsname: string]: any;
}
//页面管理 获取数据
export const pageList = () =>
  request('/api/api/page', {
    method: 'get',
    skipErrorHandler: true,
  });
//删除
export const DelPageList = (id: string) =>
  request(`/api/api/page/${id}`, {
    method: 'delete',
    skipErrorHandler: true,
  });
//发布
export const PubPageList = (params: Params) => {
  const { id, data } = params;
  return request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data,
    skipErrorHandler: true,
  });
};
//下线
export const DraftPageList = (params: Params) => {
  const { id, data } = params;
  return request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data,
    skipErrorHandler: true,
  });
};
//搜索
export const SearchPageList = (params: Params) =>
  request('/api/api/page', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
