import { request } from 'umi';

export const labelList = () => {
  return request('/api/api/tag', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
