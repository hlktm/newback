import { request } from 'umi';

export const searchList = () => {
  return request('/api/api/search', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
