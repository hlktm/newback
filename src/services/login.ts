import { request } from 'umi';

export interface loginType {
  name: string;
  password: string;
}

// export const login = (data: loginType) => {
//   console.log(data);

//   return request('/api/api/user', {
//     method: 'get',
//     data,
//     skipErrorHandler: true,
//   });
// };

// 登录
export const login = (values: any) => {
  // console.log(values);

  return request('/api/api/auth/login', {
    method: 'post',
    data: values,
    skipErrorHandler: true,
  });
};
// 注册
export const register = (values: any) => {
  console.log(values, 'values');
  return request('/api/api/user/register', {
    method: 'POST',
    data: values,
    skipErrorHandler: true,
  });
};

// 更新
export const userUpdata = (values: any) => {
  console.log(values, 'values');
  return request('/api/api/user/update', {
    method: 'POST',
    data: values,
    skipErrorHandler: true,
  });
};
