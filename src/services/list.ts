import { request } from 'umi';

export interface Params {
  i18n: string;
  systemUrl: string;
  adminSystemUrl: string;
  systemTitle: string;
  systemLogo: string;
  systemFavicon: string;
  systemFooterInfo: string;
  seoKeyword: string;
  seoDesc: string;
  baiduAnalyticsId: string;
  googleAnalyticsId: null;
}

export const list = () => {
  return request('/api/api/article', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
