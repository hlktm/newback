import { request } from 'umi';
interface Params {
  page?: number;
  pageSize?: number;
  [propsname: string]: any;
}
//获取评论列表
export const commentList = (params: Params) =>
  request('/api/api/comment', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
//评论页面删除
export const DelCommentList = (id: any) =>
  request(`/api/api/comment/${id}`, {
    method: 'delete',
    // params,
    skipErrorHandler: true,
  });
//通过
export const PassCommentList = (params: Params) => {
  const { id, data } = params;
  return request(`/api/api/comment/${id}`, {
    method: 'PATCH',
    data,
    skipErrorHandler: true,
  });
};
//拒绝
export const RefuseCommentList = (params: Params) => {
  const { id, data } = params;
  return request(`/api/api/comment/${id}`, {
    method: 'PATCH',
    data,
    skipErrorHandler: true,
  });
};
//搜索
export const SearchCommentList = (params: Params) =>
  request('/api/api/comment', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
