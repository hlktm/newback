import { request } from 'umi';

// 评论数据
export const commentList = () => {
  return request('/api/api/comment', {
    method: 'GET',
    skipErrorHandler: true,
  });
};

// 修改评论数据
export const commentChange = (text: any) => {
  // console.log(text, 'id');
  return request(`/api/api/comment/${text.id}`, {
    method: 'PATCH',
    data: text,
    skipErrorHandler: true,
  });
};

// 删除评论数据
export const commentDel = (text: any) => {
  console.log(text, 'id');
  return request(`/api/api/comment/${text.id}`, {
    method: 'delete',
    params: text,
    skipErrorHandler: true,
  });
};
