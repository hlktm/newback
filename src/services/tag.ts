import { request } from 'umi';
export const TagList = () => {
  return request('/api/api/tag', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
