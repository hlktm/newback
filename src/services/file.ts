import { request } from 'umi';

// // 文件数据
export const fileList = (params: any) => {
  // console.log(params, 'params');

  return request('/api/api/file', {
    method: 'GET',
    params,
    skipErrorHandler: true,
  });
};

// // 文件数据
// export const fileList = () => {

//   return request('/api/api/file', {
//     method: 'GET',
//     skipErrorHandler: true,
//   });
// };
