import { request } from 'umi';

export const categoryList = () => {
  return request('/api//api/category', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
