import { request } from 'umi';

export const postersList = () => {
  return request('/api/api/article', {
    method: 'GET',
    skipErrorHandler: true,
  });
};

// 海报管理上传
export const postersUpload = () => {
  return request('/api/api/file/upload', {
    method: 'POST',
    skipErrorHandler: true,
  });
};
