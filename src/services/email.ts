import { request } from 'umi';

//  邮箱数据
export const emailList = () => {
  return request('/api/api/smtp', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
