import { request } from 'umi';
// 获取知识小册
export const knowledgeList = () =>
  request('/api/api/knowledge', {
    method: 'get',
    skipErrorHandler: true,
  });
//知识小册删除
export const DelKnowList = (id: any) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'delete',
    // params,
    skipErrorHandler: true,
  });
