import { request } from 'umi';
import { Params } from './list';

// 获取文章数据
export interface allType {
  [propsName: string]: any;
}
export const ArticleList = (params: allType) => {
  // console.log(params, 'params');
  return request('/api/api/article', {
    method: 'GET',
    params,
    skipErrorHandler: true,
  });
};
// 首焦
export const isRecommended = (params: allType) => {
  const { id, isRecommended } = params;
  // console.log(params,"params");

  return request(`/api/api/article/${id}`, {
    method: 'PATCH',
    data: { isRecommended: isRecommended },
    skipErrorHandler: true,
  });
};
//
export const action = (params: allType) => {
  // console.log(params, 'params');
  const { id } = params;
  return request(`/api/api/article/${id}`, {
    method: 'GET',
    // data: { id },
    skipErrorHandler: true,
  });
};
// 删除
export const delList = (params: allType) => {
  const { id } = params;
  console.log(params, 'params');

  return request(`/api/api/article/${params}`, {
    method: 'DELETE',
    skipErrorHandler: true,
  });
};
// 发布

export const publishData = (params: allType) => {
  const { id } = params;
  console.log(params, 'params');

  return request(`/api/api/article/${id}`, {
    method: 'PATCH',
    data: { status: 'publish' },
    skipErrorHandler: true,
  });
};
// 发布
export const draftData = (params: allType) => {
  const { id } = params;
  console.log(params, 'params');

  return request(`/api/api/article/${id}`, {
    method: 'PATCH',
    data: { status: 'draft' },
    skipErrorHandler: true,
  });
};
