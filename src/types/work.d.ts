import { IndexModelState } from 'umi';

export interface PageProps {
  index: IndexModelState;
  loading: boolean;
}

export interface DataType {
  pass?: any;
  content?: string;
  key?: string;
  name?: string;
  age?: number;
  address?: string;
  tags?: string[];
}

export interface ListType {
  data: ItemType;
}
export interface ItemType {
  [title: string]: any;
}

// dva的命名空间
export interface dvaNameType {
  work: string;
}

// 工作台dva仓库的数据
export interface dvaStateType {
  name: string;
  workComment: Array<dvaItemType>;
  workCommentLength: number;
}
//评论的数据
export interface dvaItemType {
  content: string;
  createAt: string;
  email: string;
  hostId: string;
  html: string;
  id: string;
  name: string;
  parentCommentId: null;
  pass: true;
  replyUserEmail: null;
  replyUserName: null;
  updateAt: string;
  url: string;
  userAgent: string;
}
